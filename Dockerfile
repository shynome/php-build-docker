ARG PHP_VERSION=7.2

FROM php:${PHP_VERSION}-alpine

# installl composer
RUN php -r "readfile('https://getcomposer.org/installer');" | php -- --install-dir=/bin --filename=composer

ARG COMPOSER_HOME=/usr/local/composer

ENV COMPOSER_HOME=${COMPOSER_HOME} \
    PATH="${COMPOSER_HOME}/vendor/bin:${PATH}"

# add phplint
RUN composer global require -n overtrue/phplint
# 查看版本号以验证是否正确安装
RUN phplint --version
