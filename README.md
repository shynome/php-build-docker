## 用途

用以在上线部署前检查 `php` 是否有低级错误, 目前只提供了 `5.6` 和 `7.2` 的版本, 添加的组件也不多

## 额外添加的组件

#### [`overtrue/phplint`](https://github.com/overtrue/phplint)

作用检查 `php` 错误文件, 只是简单检查有没有语法错误之类的, 实际上并没有编辑器里的检查那么严格.
但是如果这个都过不了的话,就不应该上线
  
`drone` 使用案例
```yml
pipeline:
  phplint:
    # 选择对应的版本, 没有的话提下 issue 或者自己动手
    image: shynome/php-build:7.2
    commands:
    # `-n` 选项表示不要进行交互
    - phplint -n
```